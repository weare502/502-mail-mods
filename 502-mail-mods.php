<?php
/**
 * Plugin Name: 502 Mail Mods
 * Description: Applies modifications relating to emails sent throughout the site.
 * Author: 502 Media Group
 * Author URI: http://502mediagroup.com
 * Version: 1.0
 */
add_filter('retrieve_password_message', function( $message ){

	$message = str_replace('<', '(', $message);
	$message = str_replace('>', ')', $message);

	return $message;

}, 99, 1 );